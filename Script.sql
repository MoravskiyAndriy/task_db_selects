CREATE SCHEMA IF NOT EXISTS `privat_DB` DEFAULT CHARACTER SET utf8 ;
USE `privat_DB` ;

set time_zone = '+06:00';

DROP TABLE IF EXISTS `privat_DB`.`transaction`;
DROP TABLE IF EXISTS `privat_DB`.`operation`;
DROP TABLE IF EXISTS `privat_DB`.`account`;
DROP TABLE IF EXISTS `privat_DB`.`deposit`;
DROP TABLE IF EXISTS `privat_DB`.`user_has_credit`;
DROP TABLE IF EXISTS `privat_DB`.`credit`;
DROP TABLE IF EXISTS `privat_DB`.`password`;
DROP TABLE IF EXISTS `privat_DB`.`user`;
DROP TABLE IF EXISTS `privat_DB`.`currency`;

CREATE TABLE IF NOT EXISTS `privat_DB`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(30) NOT NULL,
  `surname` VARCHAR(30) NOT NULL,
  `patronymic` VARCHAR(30) NULL DEFAULT NULL,
  `email` VARCHAR(45) NULL DEFAULT NULL,
  `phone_number` VARCHAR(45) NOT NULL,
  `settlement` VARCHAR(45) NOT NULL,
  `street` VARCHAR(45) NOT NULL,
  `house_number` INT NOT NULL,
  `flat_number` INT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `phone_number_UNIQUE` (`phone_number` ASC) VISIBLE)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `privat_DB`.`password` (
  `user_id` INT NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_password_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `privat_DB`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `privat_DB`.`currency` (
  `id` TINYINT(2) NOT NULL,
  `currency_val` VARCHAR(5) NOT NULL,
  `to_uah_modifier` DECIMAL(15,5) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `privat_DB`.`account` (
  `user_id` INT NOT NULL,
  `number` BIGINT(20) NOT NULL,
  `balance` DECIMAL(15,2) NOT NULL DEFAULT 0,
  `currency_id` TINYINT(2) NOT NULL,
  INDEX `fk_account_user1_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`number`),
  INDEX `fk_currency_idx` (`currency_id` ASC) VISIBLE,
  UNIQUE INDEX `number_UNIQUE` (`number` ASC) VISIBLE,
  CONSTRAINT `fk_account_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `privat_DB`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_currency_id`
    FOREIGN KEY (`currency_id`)
    REFERENCES `privat_DB`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `privat_DB`.`deposit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `sum` DECIMAL(15,2) NOT NULL DEFAULT 0,
  `currency_id` TINYINT(2) NOT NULL,
  `rate` DECIMAL(10,2) NOT NULL,
  `opening_date` DATE NOT NULL,
  `closing_date` DATE NOT NULL,
  INDEX `fk_deposit_user_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  INDEX `fk_depcurrency_idx` (`currency_id` ASC) VISIBLE,
  UNIQUE INDEX `number_UNIQUE` (`id` ASC) VISIBLE,
  CONSTRAINT `fk_deposit_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `privat_DB`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_dep_currency_id`
    FOREIGN KEY (`currency_id`)
    REFERENCES `privat_DB`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `privat_DB`.`credit` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `sum` DECIMAL(15,2) NOT NULL DEFAULT 0,
  `currency_id` TINYINT(2) NOT NULL,
  `rate` DECIMAL(10,2) NOT NULL,
  `opening_date` DATE NOT NULL,
  `closing_date` DATE NOT NULL,
  UNIQUE INDEX `number_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_credit_currency_id` (`currency_id` ASC) VISIBLE,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_credit_currency_id`
    FOREIGN KEY (`currency_id`)
    REFERENCES `privat_DB`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

CREATE TABLE IF NOT EXISTS `privat_DB`.`operation` (
  `id` TINYINT(2) NOT NULL,
  `operation_type` VARCHAR(45) NOT NULL,
  `commission` DECIMAL(15,2) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;

CREATE TABLE IF NOT EXISTS `privat_DB`.`transaction` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `account_number_this` BIGINT(20) NOT NULL,
  `operation_commentary` VARCHAR(50) NOT NULL,
  `date` TIMESTAMP NOT NULL,
  `sum` DECIMAL(15,2) NOT NULL,
  `currency_id` TINYINT(2) NOT NULL,
  `operation_id` TINYINT(2) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_operation_idx` (`operation_id` ASC) VISIBLE,
  INDEX `fk_account_number` (`account_number_this` ASC) VISIBLE,
  INDEX `fk_currency_id_transaction` (`currency_id` ASC) VISIBLE,
  CONSTRAINT `fk_account_number`
    FOREIGN KEY (`account_number_this`)
    REFERENCES `privat_DB`.`account` (`number`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_currency_id_transaction`
    FOREIGN KEY (`currency_id`)
    REFERENCES `privat_DB`.`currency` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_operation_id`
    FOREIGN KEY (`operation_id`)
    REFERENCES `privat_DB`.`operation` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
KEY_BLOCK_SIZE = 8;

CREATE TABLE IF NOT EXISTS `privat_DB`.`user_has_credit` (
  `user_id` INT NOT NULL,
  `credit_id` INT NOT NULL,
  PRIMARY KEY (`user_id`, `credit_id`),
  INDEX `fk_user_has_credit_credit1_idx` (`credit_id` ASC) VISIBLE,
  INDEX `fk_user_has_credit_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_has_credit_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `privat_DB`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_user_has_credit_credit1`
    FOREIGN KEY (`credit_id`)
    REFERENCES `privat_DB`.`credit` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

insert into currency values('1','UAH','1.0');
insert into currency values('2','USD','0.04');
insert into currency values('3','EUR','0.037037037');

insert into operation values('1','replenishment of bank account','0.0');
insert into operation values('2','interaccount transfer','5.0');
insert into operation values('3','replenishment of mobile account','1.0');
insert into operation values('4','interbank transfer','10.0');
insert into operation values('5','tickets purchase','0.0');
insert into operation values('6','deposit operation','0.0');
insert into operation values('7','credit operation','0.0');

insert into user (name,surname,patronymic,email,phone_number,settlement,street,house_number) values('Andriy','Moravskiy','Andriyovych','myemail@gmail.com','0631111111','Zhowkva','MyStreet','5');
insert into user (name,surname,email,phone_number,settlement,street,house_number) values('Stewie','Griffin','stewie@gmail.com','0632221111','Quahog','Spooner Street','6');
insert into user (name,surname,email,phone_number,settlement,street,house_number) values('Brian','Griffin','brian@gmail.com','0633331111','Quahog','Spooner Street','6');

insert into password values('1','knwsx');
insert into password values('2','czyyxobmrsvn');
insert into password values('3','czyyxobnyq');

insert into account values('1','1000000000000001','1000','1');
insert into account values('1','1000000000000002','5000','1');
insert into account values('2','1000100000000003','10000','2');
insert into account values('3','1000100000000004','500','3');

insert into credit values('1','100','1','20','2019-10-27','2019-11-27');
insert into credit values('2','20','1','20','2019-10-27','2019-12-27');
insert into credit values('3','200','1','20','2019-10-27','2019-11-27');
insert into credit values('4','1000','1','20','2019-10-27','2019-11-27');

insert into transaction values('1','1000000000000001','0631111111','2019-10-27 14:10:00','20',1,3);

insert into user_has_credit values(1,1);
insert into user_has_credit values(1,2);
insert into user_has_credit values(2,1);
insert into user_has_credit values(3,3);
insert into user_has_credit values(3,4);
